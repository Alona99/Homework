package ua.org.lesson.one.homework2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by admin on 11.11.2016.
 */
public class TestCentralGarage {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
          CentralGarage centralGarage=new CentralGarage();
        Van van1=new Van(12,"Reno");
        Van van2=new Van(1,"Volvo");
        Van van3=new Van(14,"BMV");
        Van van4=new Van(2,"Ford");

        centralGarage.addVanToCentralGarage(van1);
        centralGarage.addVanToCentralGarage(van2);
        centralGarage.addVanToCentralGarage(van3);
        centralGarage.addVanToCentralGarage(van4);
        System.out.println("The number of vans in the garage" + centralGarage.allVans.size());
        centralGarage.calculateAveregeCapacity();
        centralGarage.showVansAboveAverage();
        centralGarage.showVansBelowAverage();
        System.out.println("Input carrying your capacity of a car");
        int carryingCapacity = Integer.parseInt(br.readLine());
        centralGarage.setTargetCapacity(carryingCapacity);
        centralGarage.showVansAboveInput();
        centralGarage.showVansBelowInput();
    }

}
