package ua.org.lesson.one.homework2;

import java.util.Scanner;

/**
 * Created by admin on 11.11.2016.
 */
public class Van {
    private int carryingCapacity;
    private String makeOfTheCar;
    private final int Min_Capacity = 1;
    private final int Max_Capacity = 14;

    public Van(int carryingCapacity, String makeOfTheCar) {
        setCarryingCapacity(carryingCapacity);
        setMakeOfTheCar(makeOfTheCar);
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        if(carryingCapacity >= Min_Capacity && carryingCapacity <= Max_Capacity) {
            this.carryingCapacity =carryingCapacity;
         }else{
            System.out.println("The capacity isn`t correct.Use capacity from" + Min_Capacity + "to"+Max_Capacity);;
        }
    }

    public String getMakeOfTheCar() {
        return makeOfTheCar;
    }

    public void setMakeOfTheCar(String makeOfTheCar) {
        this.makeOfTheCar = makeOfTheCar;
    }

    @Override
    public String toString() {
        return "Van{" +
                "carryingCapacity=" + carryingCapacity +
                ", makeOfTheCar='" + makeOfTheCar + '\'' +
                '}';
    }
    public Van()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter make of the car");
        setMakeOfTheCar(in.nextLine());
        System.out.println("Enter the capacity of the car");
        setCarryingCapacity(in.nextInt());

             }

}
