package ua.org.lesson.one.homework2;

import java.util.ArrayList;

/**
 * Created by admin on 11.11.2016.
 */
public class CentralGarage {
    private int targetCapacity;
    private int averageCapacity;
    private int sumCapacityOfAllVans;
    static ArrayList<Van> allVans= new ArrayList<Van>();

    public int getTargetCapacity() {
        return targetCapacity;
    }

    public void setTargetCapacity(int targetCapacity) {
        this.targetCapacity = targetCapacity;
    }

    public int getAverageCapacity() {
        return averageCapacity;
    }

    public void setAverageCapacity(int averageCapacity) {
        this.averageCapacity = averageCapacity;
    }

    public int getSumCapacityOfAllVans() {
        return sumCapacityOfAllVans;
    }

    public void setSumCapacityOfAllVans(int sumCapacityOfAllVans) {
        this.sumCapacityOfAllVans = sumCapacityOfAllVans;
         }
         public void calculateAveregeCapacity(){
             sumCapacityOfAllVans=0;
             for(int i=0;i<allVans.size();i++){
                 sumCapacityOfAllVans+= allVans.get(i).getCarryingCapacity();
             }
             averageCapacity=sumCapacityOfAllVans/allVans.size();
             System.out.println("Average capacity is " + averageCapacity);
         }
         public void showVansAboveAverage(){
             System.out.println("All vans, which capacity is above average");
             for(Van el : allVans){
                 if(el.getCarryingCapacity()>averageCapacity){
                     System.out.println(el.getMakeOfTheCar());
                     System.out.println();
                 }
             }
         }
         public void showVansBelowAverage(){
             System.out.println("All vans, which capacity is below average");
             for (Van el : allVans){
                 if(el.getCarryingCapacity()<averageCapacity){
                     System.out.println(el.getMakeOfTheCar());
                     System.out.println();
                 }
             }
         }
         public void showVansAboveInput(){
             System.out.println("Vans, which capacity is above input capacity");
             for(Van el : allVans){
                 if( el.getCarryingCapacity()>targetCapacity){
                     System.out.println(el.getMakeOfTheCar());
                     System.out.println();
                 }
             }
         }
    public void showVansBelowInput(){
        System.out.println("Vans, which capacity is above input capacity");
        for(Van el : allVans){
            if( el.getCarryingCapacity()<targetCapacity){
                System.out.println(el.getMakeOfTheCar());
                System.out.println();
            }
        }
    }
    public void addVanToCentralGarage(Van addingVan){
        allVans.add(addingVan);

    }
}
